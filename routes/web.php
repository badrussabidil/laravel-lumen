<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    $res['succcess'] = true;
    $res['result'] = "helo badrus";
    return response($res);
});

$router->post('/login', 'LoginController@index');
$router->post('/register', 'UserController@register');
$router->post('/user{id}', ['middleware' => 'auth', 'user' => 'UserController@getUser']);

// $router->post('/', function () use ($router) {
//     return $router->app->version();
// });

// $router->delete('/', function () use ($router) {
//     return $router->app->version();
// });


// $router->put('/', function () use ($router) {
//     return $router->app->version();
// });
